<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>New Zealand Regions - Interactive</title>
<style type="text/css" media="screen">
body {
	background: #333;
	color: #fff;
	font: 300 100.1% "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial,
		sans-serif;
}

#holder {
	height: 480px;
	left: 50%;
	margin: -240px 0 0 -320px;
	position: absolute;
	top: 50%;
	width: 640px;
}

#copy {
	bottom: 0;
	font: 300 .7em "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial,
		sans-serif;
	position: absolute;
	right: 1em;
	text-align: right;
}

#copy a {
	color: #fff;
}

#canvas {
	height: 400px;
	left: 40%;
	margin: -150px 0 0 -300px;
	position: absolute;
	top: 40%;
	width: 900px;
}

#paper {
	height: 400px;
	left: 0;
	position: absolute;
	top: 0;
	width: 400px;
}

.region-desc {
	display: none;
	height: 400px;
	overflow: auto;
	position: absolute;
	right: 0;
	top: 0;
	width: 500px;
}

h2 {
	text-align: center;
}
</style>
<script src="js/raphael.js" type="text/javascript" charset="utf-8"></script>
<script src="js/nzgeography.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24495155-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
	<div id="canvas">
		<div id="paper"></div>
		<div id="northland" class="region-desc">
			<h2>Northland (Te Tai-tokerau)</h2>
			<p>The Northland Region (Māori: Te Tai-tokerau, also Te
				Hiku-o-te-Ika, 'the Tail of the Fish (of Maui)'), one of the 16
				regions of New Zealand, is, as the name suggests, the northernmost
				of New Zealand's administrative regions. The main centre is the city
				of Whangarei.
				<br /><br />
				Northland is located in what is often referred to by
				New Zealanders as the Far North, or, because of its mild climate,
				The Winterless North. It occupies the upper 80% of the 285
				kilometre-long North Auckland Peninsula, the southernmost part of
				which is in the Auckland Region.</p>
		</div>
		<div id="waikato" class="region-desc">
			<h2>Waikato</h2>
			<p>The Waikato Region is a local government region of the upper North
				Island of New Zealand. It covers the Waikato, Hauraki, Coromandel
				Peninsula, the northern King Country, much of the Taupo District,
				and parts of Rotorua District. It is governed by the Waikato
				Regional Council, which administers the region under the name
				Environment Waikato. 
				<br /><br />
				The name for the region is taken from the Waikato River; waikato is 
				a Māori word traditionally translated as "flowing water" (specifically, 
				wai = "water" and kato = "the pull of the river current in the sea").</p>
		</div>
		<div id="auckland" class="region-desc">
			<h2>Auckland (Tāmaki Makaurau)</h2>
			<p>The Auckland metropolitan area in the North Island of New Zealand,
				is the largest and most populous urban area in the country with
				1,354,900 residents, 31 percent of the country's population.
				Auckland also has the largest Polynesian population of any city in
				the world. 
				<br /><br />
				In Māori Auckland's name is Tāmaki Makaurau, or the
				transliterated version of Auckland, Ākarana. Auckland lies between
				the Hauraki Gulf of the Pacific Ocean to the east, the low Hunua
				Ranges to the south-east, the Manukau Harbour to the south-west, and
				the Waitakere Ranges and smaller ranges to the west and north-west.
				<br /><br />
				The central part of the urban area occupies a narrow isthmus between
				the Manukau Harbour on the Tasman Sea and the Waitemata Harbour on
				the Pacific Ocean. It is one of the few cities in the world to have
				harbours on two separate major bodies of water.</p>
		</div>
		<div id="manawatu" class="region-desc">
			<h2>Manawatu-Wanganui</h2>
			<p>The region covers all or part of ten Districts. Parts of five of
				these are covered by five other regions of New Zealand, the most of
				any region. In descending order of land area the Districts are
				Ruapehu, Tararua (part), Rangitikei (part), Manawatu, Wanganui,
				Horowhenua, Stratford (part), Palmerston North, Waitomo (part) and
				Taupo (part). The region is dominated and defined by two significant
				river catchments, the Whanganui and the Manawatu. 
				<br /><br />
				The Whanganui River, in the region's northwest, is the longest 
				navigable river in New Zealand. The river was extremely important to 
				early Māori as it was the southern link in a chain of waterways that 
				spanned almost two-thirds of the North Island. It was one of the chief 
				areas of Māori settlement with its easily fortified cliffs and ample 
				food supplies. Legends emphasise the importance of the river and it
				remains sacred to Wanganui iwi.</p>
		</div>
		<div id="bayofplenty" class="region-desc">
			<h2>Bay of Plenty (Te Moana-a-Toi)</h2>
			<p>The Bay of Plenty is a large indentation in the northern coast of
				New Zealand's North Island. It stretches from the Coromandel
				Peninsula in the west to Cape Runaway in the east, a wide stretch of
				some 259 km of open coastline. The Bay of Plenty Region is situated
				around this body of water, also incorporating several large islands
				in the bay. 
				<br /><br />
				The name "Bay of Plenty" originated with James Cook during his 1769–70 
				exploration of New Zealand, who noted the abundant resources in the area. 
				The Māori name for the bay is Te Moana-a-Toi ("the sea of Toi"), a 
				reference to the ancestral explorer Toi-te-huatahi.</p>
		</div>
		<div id="eastcape" class="region-desc">
			<h2>Gisborne (Tai Rāwhiti)</h2>
			<p>The Gisborne Region, commonly referred to as the East Cape, is an
				area of northeastern New Zealand governed by the Gisborne District
				Council, a unitary authority. Its largest settlement, the city also
				named Gisborne, is located at the northern end of Poverty Bay on the
				east coast. 
				<br /><br />
				The region's population has higher than the national average proportion 
				of Māori, over 50% in some areas, and still maintains strong ties to 
				both Māori tradition and the iwi and marae structure. The predominant 
				iwi in the region are Ngāti Porou, Rongowhakaata, Ngai Tamanuhiri, 
				Te Aitanga a Mahaki.</p>
		</div>
		<div id="hawkesbay" class="region-desc">
			<h2>Hawke's Bay (Heretaunga)</h2>
			<p>Hawke's Bay is a region of New Zealand. Hawke's Bay is recognised
				on the world stage for its award-winning wines. The regional council
				sits in both the cities of Napier and Hastings. The region is
				situated on the east coast of the North Island. 
				<br /><br />
				The region bears the former name of what is now Hawke Bay, a large 
				semi-circular bay which extends for 100 kilometres from northeast to 
				southwest from Mahia Peninsula to Cape Kidnappers.
				<br /><br /> 
				One trivial fact is that the region has a hill with the longest place 
				name in New Zealand:
				
				Taumata­whakatangihanga­koauau­o­tamatea­turi­pukakapiki­maunga­horo­nuku­pokai­whenua­kitanatahu 
				is an unremarkable hill in southern Hawke's Bay, not far from Waipukurau.</p>
		</div>
		<div id="taranaki" class="region-desc">
			<h2>Taranaki</h2>
			<p>Taranaki is a region in the west of New Zealand's North Island and is 
				the 10th largest region of New Zealand by population. It is named for the 
				region's main geographical feature, Mount Taranaki.
				<br /><br />
				Mount Taranaki, or Mount Egmont—Te Maunga O Taranaki in Māori—is the dominant 
				feature of the region, second-tallest mountain in the North Island. Māori legend 
				says that Taranaki previously lived with the Tongariro, Ngauruhoe and Ruapehu 
				mountains of the central North Island but fled to its current location after a 
				battle with Tongariro.
				</p>
		</div>
		<div id="wellington" class="region-desc">
			<h2>Wellington (Te Upoko-o-te-Ika)</h2>
			<p>The official Wellington Region, as administered by the Wellington Regional Council 
				(under the brand-name "Greater Wellington") covers the conurbation around the capital 
				city, Wellington, and the cities of Lower Hutt, Porirua, and Upper Hutt, each of which 
				also contains a rural hinterland. It extends up the west coast of the North Island, 
				taking in the coastal settlements of the Kapiti Coast district. 
				<br /><br />
				In Māori, Wellington goes by three names. Te Whanga-nui-a-Tara refers to Wellington 
				Harbour and means "the great harbour of Tara". Pōneke is a transliteration of Port 
				Nick, short for Port Nicholson (the city's central marae, the community supporting it and 
				its kapa haka have the pseudo-tribal name of Ngāti Pōneke). Te Upoko-o-te-Ika-a-Māui, 
				meaning The Head of the Fish of Māui (often shortened to Te Upoko-o-te-Ika), a traditional 
				name for the southernmost part of the North Island, derives from the legend of the fishing 
				up of the island by the demi-god Māui.</p>
		</div>
		<div id="canterbury" class="region-desc">
			<h2>Canterbury (Waitaha)</h2>
			<p>Canterbury is New Zealand's largest region by area, with an area of 45,346 km². The region 
				is traditionally bounded in the north by the Conway River and to the west by the Southern Alps. 
				The southern boundary is the Waitaki River.
				<br /><br />
				The area is commonly divided into North Canterbury (north of the Rakaia River), Mid Canterbury 
				(from the Rakaia River to the Rangitata River), South Canterbury (south of the Rangitata River) 
				and Christchurch (Christchurch City). For many purposes South Canterbury is considered a separate 
				region, centred on the city of Timaru.</p>
		</div>
		<div id="marlborough" class="region-desc">
			<h2>Marlborough (Marapara)</h2>
			<p>Marlborough's geography can be roughly divided into four sections. Two of these sections, 
				in the south and the west, are mountainous. This is particularly true of the southern section, 
				which rises to the peaks of the Kaikoura Ranges. These two mountainous regions are the final 
				northern vestiges of the ranges that make up the Southern Alps, although that name is rarely 
				applied to mountains this far north.
				<br /><br />
				Between these two areas is the long straight valley of the Wairau River. This broadens to 
				wide plains at its eastern end, in the centre of which stands the town of Blenheim. This 
				region has fertile soil and temperate weather, and as such has become a centre of the New 
				Zealand wine industry.</p>
		</div>
		<div id="westcoast" class="region-desc">
			<h2>The West Coast (Te Tai Poutini)</h2>
			<p>The West Coast region reaches from Kahurangi Point in the north to Awarua Point in the south, 
				a distance of 600 km. To the west is the Tasman Sea (which like the Southern Ocean is known to 
				be very rough, with 4 metre swells being common), and to the east are the Southern Alps. Much 
				of the land is rugged, although there are coastal plains around which much of the population 
				resides.
				<br /><br />
				The land is very scenic, with wild coastlines, mountains, and a very high proportion of native 
				bush, much of it native temperate rain forest. The West Coast is the only part of New Zealand 
				where significant tracts of lowland forest remain-elsewhere, for instance on the Canterbury 
				Plains and in the Firth of Thames, they have been almost completely destroyed for settlement 
				and agriculture. Scenic areas include the Haast Pass, Fox and Franz Josef Glaciers, the 
				Pancake Rocks at Punakaiki and the Heaphy Track.</p>
		</div>
		<div id="otago" class="region-desc">
			<h2>Otago (Otakou)</h2>
			<p>The name "Otago" is an old southern Maori word whose North Island dialect equivalent is 
				"Otakou", introduced to the south by Europeans in the 1840s. The exact meaning of the 
				term is disputed, with common translations being "isolated village" and "place of red earth", 
				the latter referring to the reddish-ochre clay which is common in the area around Dunedin. 
				"Otago" is also the old name of the European settlement on the Otago Harbour, established by the 
				Weller Brothers in 1831. The place later became the focus of the Otago Association, an offshoot 
				of the Free Church of Scotland, notable for its high-minded adoption of the principle that 
				ordinary people, not the landowner, should choose the ministers.
				<br /><br />
				New Zealand's first university, The University of Otago, was founded in 1869 as the provincial 
				university in Dunedin. The Central Otago wine area produces award winning wines made from 
				varieties such as the Pinot Noir, Chardonnay, Sauvignon Blanc, Merlot, and Riesling grapes. 
				Central Otago has an increasing reputation as New Zealand’s leading pinot noir region.</p>
		</div>
		<div id="southland" class="region-desc">
			<h2>Southland (Murihiku)</h2>
			<p>Southland was a scene of early extended contact between Europeans and Maori, in this case 
				sealers, whalers and missionaries - Wohlers at Ruapuke. In 1853, Walter Mantell purchased 
				Murihiku from local Maori iwi, claiming the land for European settlement. Over successive 
				decades, present-day Southland and Otago were settled by large numbers of Scottish settlers. 
				Immigration to New Zealand had been precipitated by an economic depression in Scotland and 
				a schism between the Church of Scotland and the Free Church of Scotland.
				<br /><br />
				Southland contains New Zealand's highest waterfall—the Browne Falls. Lake Hauroko is the 
				deepest lake in the country. The highest peak in Southland is Mount Tutoko, which is part of 
				the Darran mountains. The largest lake in Southland is Lake Te Anau followed by Lake Manapouri 
				which both lie within the boundaries of Fiordland National Park. Established on 20 February 1905, 
				it is the largest national park in New Zealand—covering much of Fiordland which is devoid of 
				human settlement.</p>
		</div>
		<div id="nelson" class="region-desc">
			<h2>Nelson (Whakatū)</h2>
			<p>The Nelson Tasman or "Top of the South" region is administered as two unitary authorities by 
				Nelson City Council and the (much larger in geographical area) adjoining Tasman District Council, 
				headquartered in Richmond 15 kilometres to the south west. It is between Marlborough, another 
				unitary authority, to the east, and the West Coast Regional Council to the west.
				<br /><br />
				Nelson has beaches and a sheltered harbour. The harbour entrance is protected by a Boulder Bank, 
				a natural, 13 km bank of rocks transported south from Mackay Bluff via longshore drift. The bank 
				creates a perfect natural harbour which enticed the first settlers although the entrance was 
				narrow. The wreck of the Fifeshire on Arrow Rock (now called Fifeshire Rock in memory of this 
				disaster) in 1842 proved the difficulty of the passage. A cut was later made in the bank in 
				1906 which allowed larger vessels access to the port.</p>
		</div>
		<div id="tasman" class="region-desc">
			<h2>Tasman (Te Tau Ihu o Te Waka a Maui)</h2>
			<p>Tasman District is a large area at the top western side of the South Island of New Zealand. 
				It covers 9,786 square kilometres and is bounded to the west by the Matiri Ranges, Tasman 
				Mountains and the Tasman Sea. To the north Tasman and Golden Bays form its seaward edge, 
				and the eastern boundary extends to the edge of Nelson city, and includes part of the Spencer 
				Mountains and the Saint Arnaud and Richmond Ranges. The Victoria Ranges form Tasman's southern 
				boundary and the district's highest point is Mount Franklin, at 2,340 metres.
				<br /><br />
				According to tradition, the Māori waka Uruao, brought ancestors of the Waitaha people to 
				Tasman in the 12th Century. Archaeological evidence suggests the first Māori settlers explored 
				the region thoroughly, settling mainly along the coast where there was ample food.
				The succession of tribes into the area suggests considerable warfare interrupted their lives. 
				</p>
		</div>
	</div>
	<p id="copy">
		Geo-map produced using <a href="http://raphaeljs.com/">Raphaël</a> - Information from Wikipedia - Design by Alex Gibson, Jamie Love and Raphaël.
	</p>
</body>
</html>
